def extra_config_files(services):
    res = []
    for service in services.values():
        for template in service.get('service_config', []):
            res.append({
                "src": f"dockercompose/{service['name']}/{template}",
                "dest": f"/etc/{service['name']}/{template}",
            })

    return res


def nginx_config_files(services):
    res = []
    for service in services.values():
        if 'nginx_config' in service:
            res.append({
                "src": f"dockercompose/{service['name']}/{service['nginx_config']}",
                "dest": f"/etc/nginx/vhosts.d/{service['name']}.conf",
            })

    return res


class FilterModule():
    def filters(self):
        return {
            'extra_config_files': extra_config_files,
            'nginx_config_files': nginx_config_files,
        }
