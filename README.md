# ansible docker-compose role

## Usage

```
dockercompose__services:
    keycloak:
      name: keycloak
      config: |
      version: "3.3"
      services:
        keycloak:
          image: harbor.bastelgenosse.de/keycloak/keycloak:15.0.1
          ports:
          - "127.0.0.1:8080:8080"
          environment:
            KEYCLOAK_USER: bla
            KEYCLOAK_PASSWORD: foobar

            PROXY_ADDRESS_FORWARDING: "true"
          volumes:
            - "/home/user/pilotschule/keycloak-import:/tmp/export"
    moodle:
      name: moodle
      config: |
      version: "3.3"
      services:
        moodle:
          image: moodle:1.33.7
          ports:
          …
```
